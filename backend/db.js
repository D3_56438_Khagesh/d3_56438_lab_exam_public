const mysql = require('mysql2')

const openConnection = ()=>{
    const connection = mysql.createConnection({
        user: "root",
        password: "root",
        database: "moviedb"
    })

    connection.connect();
    return connection;
}

module.exports = {
    openConnection
}