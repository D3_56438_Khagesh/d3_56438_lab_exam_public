const express = require('express');
const db = require('../db')

const router = express.Router();

router.post('/add', (request, response)=>{
    const { movie_title, movie_release_date, movie_time, director_name} = request.body
    const connection = db.openConnection()

    const statement = `
    INSERT INTO TABLE movie
        (movie_title, movie_release_date, movie_time, director_name)
    values
        ('${movie_title}', '${movie_release_date}', '${movie_time}', '${director_name}')
    `
    connection.query(statement, (error, result)=>{
        connection.end()
        if(error){
            response.send('eroor')
        }
        else{
            response.send(result)
        }
    })
})

router.get('/get', (request, response)=>{
    const connection = db.openConnection()

    const statement = `
    SELECT * FROM movie
    `
    connection.query(statement, (error, result)=>{
        connection.end()
        if(error){
            response.send('eroor')
        }
        else{
            response.send(result)
        }
    })
})

router.delete('/:id', (request, response)=>{
    const {id} = request.params
    const connection = db.openConnection()

    const statement = `
    DELETE FROM TABLE movie
    WHERE
        id = '${id}'   
    `
    connection.query(statement, (error, result)=>{
        connection.end()
        if(error){
            response.send('eroor')
        }
        else{
            response.send(result)
        }
    })
})

router.put('/:id', (request, response)=>{
    const {id} = request.params
    const {director_name} = request.body
    const connection = db.openConnection()

    const statement = `
    UPDATE movie
    SET
        director_name = ${director_name}
    WHERE
        id = '${id}'   
    `
    connection.query(statement, (error, result)=>{
        connection.end()
        if(error){
            response.send('eroor')
        }
        else{
            response.send(result)
        }
    })
})
module.exports = router;